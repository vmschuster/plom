__author__ = "Andrew Rechnitzer"
__copyright__ = "Copyright (C) 2018-2019 Andrew Rechnitzer"
__credits__ = ["Andrew Rechnitzer", "Colin Macdonald", "Elvis Cai", "Matt Coles"]
__license__ = "AGPLv3"

import logging

log = logging.getLogger("tools")

from plom.client.tools.arrow import *
from plom.client.tools.box import *
from plom.client.tools.comment import *
from plom.client.tools.cross import *
from plom.client.tools.delete import *
from plom.client.tools.delta import *
from plom.client.tools.ellipse import *
from plom.client.tools.highlight import *
from plom.client.tools.image import *
from plom.client.tools.line import *
from plom.client.tools.move import *
from plom.client.tools.pen import *
from plom.client.tools.penArrow import *
from plom.client.tools.questionMark import *
from plom.client.tools.text import *
from plom.client.tools.tick import *

## move and delete commands


# Arrow stuff


# Double Arrow stuff


# Box stuff


# cross stuff


# Delta stuff


# Ellipse


# Highlight


# Line


# Pen


# Pen-with-arrows


# Question-mark


# tick-mark


# Text
